import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {FightingPage} from '../pages/fighting/fighting';
import { MaterialModule } from '@angular/material';
import {CircleProgressComponent} from '../circle/circle-progress.component'

@NgModule({
  declarations: [
    CircleProgressComponent,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FightingPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    MaterialModule.forRoot()
    // CircleProgressComponent.animate()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CircleProgressComponent,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    FightingPage
  ],
  providers: []
})
export class AppModule {}
