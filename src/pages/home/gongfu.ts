

export class GongFu{
  Name : string;
  Value : number;
  TotalTime : number;
  Methods : Array<GongFuMethod>;
  CurrentMethod : GongFuMethod;
  constructor(name : string, value : number, total : number){
    this.Name = name;
    this.Value = value;
    this.TotalTime = total;
    this.Methods = [new GongFuMethod("基本法", "各种练法", 10, 100)];
    this.CurrentMethod = this.Methods[0];
  }

}

export class GongFuMethod{
  Name : string;
  Desc : string;
  NeedTime : number;
  TotalTime : number;
  constructor(name : string, desc : string, need : number, total : number){
    this.Name = name;
    this.Desc = desc;
    this.NeedTime = need;
    this.TotalTime = total;
  }
}
