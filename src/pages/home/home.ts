import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { FightingPage, FightParam } from '../fighting/fighting';
import { GongFu, GongFuMethod } from './gongfu';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
  allGong : Array<GongFu>;
  currentGong:GongFu;
  Name : string;
  OnceTime: number;
  NoticeCount : number;
  constructor(public navCtrl: NavController) {
    this.allGong=[new GongFu("练气", 1, 100),
        new GongFu("练气1", 2, 200)];
    this.Name = "hah";
    this.currentGong = this.allGong[0];
    this.currentGong.CurrentMethod = this.currentGong.Methods[0];
    this.OnceTime = 10;
    this.NoticeCount = 1;
    console.log(this.currentGong);
  }
  updateWorkout(){
    console.log(this.currentGong);
  }
  start(){
    console.log("start to do")
    let param = new FightParam();
    param.Gong = this.currentGong;
    param.NoticeCount = this.NoticeCount;
    param.Time = this.OnceTime;
    this.navCtrl.push(FightingPage, param);
  }
}
