import { Component } from '@angular/core';

import {  NavParams  } from 'ionic-angular';
import { GongFu, GongFuMethod } from '../home/gongfu';
import { Vibration } from 'ionic-native';

@Component({
  selector: 'page-fighting',
  templateUrl: 'fighting.html'
})


export class FightingPage {
  Data : FightParam;
  AllTime : number;
  LeftTime : number;
  NoticeCount : number;
  Noticed : number;
  progress : number;
  IntervalsTime: number;
  showText : string;
  running : boolean;
  constructor(params: NavParams) {
    this.Data = params.data;
    this.AllTime = this.Data.Time * 60;
    this.LeftTime = this.AllTime;
    this.Noticed = 0;
    this.NoticeCount = this.Data.NoticeCount;
    this.IntervalsTime = Math.floor(this.AllTime/(this.NoticeCount + 1));
    this.progress = 100;
    this.showText = this.getTimeText(this.LeftTime);
    this.running = false;
    this.start();
  }

  getTimeText(timeSecond){
    return Math.floor(timeSecond/60) + '分' + timeSecond%60 + '秒';
  }
  start(){
    // console.log("vibrate finished")

    // Vibration.vibrate([2000,1000,2000]);
    // console.log("vibrate finished")
    this.running = true;
    this.run();
  }
  notice(nCount : number){
    let noticeAray = new Array<number>();
    for (let i=0;i!=nCount;i++){
      noticeAray.push(2000);
      noticeAray.push(1000);
    }
    Vibration.vibrate(noticeAray);
  }
  run(){
    setTimeout(()=> {
      if (this.running){
        if (this.LeftTime >0){
          this.LeftTime--;
          this.showText = this.getTimeText(this.LeftTime);
          this.progress = Math.floor(this.LeftTime*100/this.AllTime);
          console.log(this.LeftTime, this.NoticeCount, this.Noticed, this.IntervalsTime)
          if (this.LeftTime == ((this.NoticeCount - this.Noticed) * this.IntervalsTime)){
            this.Noticed++;
            this.notice(this.Noticed)
          }
          this.run();
        }else{
          this.running = false;
          this.showText = "完成任务";
          Vibration.vibrate(10000);
          return;
        }
      }
    }, 1000);
  }

  pause(){
    this.running = !this.running;
    if (this.running){
      this.showText = this.getTimeText(this.LeftTime);
      this.run();
    }else{
      this.showText = "暂停";
    }
  }

}

export class FightParam{
  NoticeCount : number;
  Time : number;
  Gong : GongFu;
}
